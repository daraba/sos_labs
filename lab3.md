### OРДЕНА ТРУДОВОГО КРАСНОГО ЗНАМЕНИ ФЕДЕРАЛЬНОЕ ГОСУДАРСТВЕННОЕ БЮДЖЕТНОЕ ОБРАЗОВАТЕЛЬНОЕ УЧРЕЖДЕНИЕ ВЫСШЕГО ОБРАЗОВАНИЯ МОСКОВСКИЙ ТЕХНИЧЕСКИЙ УНИВЕРСИТЕТ СВЯЗИ И ИНФОРМАТИКИ

### Факультет Информационных технологий

## Отчёт по лабораторной работе №3 по дисциплине «Современные операционные системы»

### Выполнила: 
### студентка группы М091601(72)
### Евланенкова Оксана

### Проверил: 
### Волков М.М.

### Москва 2016 г.

## Цель работы: изучение систем контроля версий на примере Git.

# Выполнение: 
* Добавить файлы лабораторной работы №2 в Git репозиторий, сделать commit.

```bash 
$ git config --global user.name "Ksusha"

$ git config --global user.email "daraba06@gmail.com"

$ cd C:/SOS

$ git init
Initialized empty Git repository in C:/SOS/.git/

$ git add lab2.docx 1.png 2.png 3.png 4.png

$ git status
On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   1.png
        new file:   2.png
        new file:   3.png
        new file:   4.png
        new file:   lab2.docx

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Titulnik_Volkov.docx
        lab1.docx

$ git commit -m "lab 2 version 1.0"
[master (root-commit) 4d6c146] lab 2 version 1.0
 5 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 1.png
 create mode 100644 2.png
 create mode 100644 3.png
 create mode 100644 4.png
 create mode 100644 lab2.docx
 ```

* Внести изменение в один из файлов лабораторной №2 и сделать commit (был изменен шрифт).

```bash
$ git add lab2.docx

$ git commit -m "lab 2 version 1.1"
[master 1c917d5] lab 2 version 1.1
 1 file changed, 0 insertions(+), 0 deletions(-)
 ```
В результате было совершено 2 commit:

```bash
$ git log
commit 1c917d500ee52b6f3929acbd67094aaee9db524f
Author: Ksusha <daraba06@gmail.com>
Date:   Thu Nov 17 01:13:41 2016 +0300

    lab 2 version 1.1

commit 4d6c1466bcb4ae3ef3586946162dd1a35476333f
Author: Ksusha <daraba06@gmail.com>
Date:   Thu Nov 17 01:06:23 2016 +0300

    lab 2 version 1.0
```

* Создать ветку, сделать commit в новой ветке (был добавлен отчет по лабораторной №1).

```bash
$ git branch lab_1_2

$ git checkout lab_1_2
Switched to branch 'lab_1_2'

$ git add lab1.docx lab2.docx

$ git commit -m "Reports for lab1 and lab2"
[lab_1_2 8348d85] Reports for lab1 and lab2
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 lab1.docx
```

* Объединить 2 ветки

```bash
$ git checkout master
Switched to branch 'master'

$ git merge lab_1_2
Updating 1c917d5..8348d85
Fast-forward
 lab1.docx | Bin 0 -> 1796533 bytes
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 lab1.docx
```

* Добавить к проекту удаленный репозиторий

```bash
$ git remote add origin https://gitlab.com/daraba/sos_labs.git

$ git push -u origin master
Counting objects: 13, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (13/13), done.
Writing objects: 100% (13/13), 1.78 MiB | 354.00 KiB/s, done.
Total 13 (delta 7), reused 0 (delta 0)
To https://gitlab.com/daraba/sos_labs.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
```



