#!/bin/bash
#algevklid.sh
a=$1 #присваиваем переменной parametr1 значение первого целого числа
b=$2 #присваиваем переменной parametr2 значение второго целого числа
script_name=$0 #присваиваем переменной script_name значение имени скрипта (алгоритм Евклида для поиска наибольшего общего делителя)
function NOD() {
while [[ (("$a" -ne 0)) && (("$b" -ne 0)) ]]
do 
	if [ "$a" -ge "$b" ] 
	then let "a=a%b"
	else let "b=b%a"
	fi
done
let "res=a+b"
echo "$res"
}

read a
read b
NOD
exit 0
